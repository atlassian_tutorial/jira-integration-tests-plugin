package com.atlassian.plugins.tutorial;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.opensymphony.user.User;

import java.util.Collection;

/**
 * Basic webwork action, that will retrieve all the projects that the current user has a particular permission in.
 * <p/>
 * For example the current user may wish to see all the project that he/she has the 'Create Issue' permission in.
 */
public class ShowUsersProjectPermissionAction extends JiraWebActionSupport
{
    private final PermissionManager permissionManager;
    private String permission;
    private Collection<Project> projects;

    public ShowUsersProjectPermissionAction(final PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    @Override
    protected void doValidation()
    {
        final User user = getRemoteUser();
        //check if a user is logged in, otherwise return with an appropriate error message
        if (user == null)
        {
            addErrorMessage("Please login to view the current user's projects for a permission.");
        }
        final int permissionType = Permissions.getType(permission);
        //then check if the permission provided as a parameter is a valid JIRA permission
        if (permissionType == -1)
        {
            addErrorMessage("Invalid permission specified.  Permission '" + permission + "' is unknown!");
        }
    }

    @Override
    protected String doExecute() throws Exception
    {
        final User user = getRemoteUser();
        final int permissionType = Permissions.getType(permission);

        //retrieve all the projects that the current user has the given permission type in.
        projects = permissionManager.getProjectObjects(permissionType, user);
        return SUCCESS;
    }

    public Collection<Project> getProjects()
    {
        return projects;
    }

    public String getPermission()
    {
        return permission;
    }

    public void setPermission(final String permission)
    {
        this.permission = permission;
    }
}
