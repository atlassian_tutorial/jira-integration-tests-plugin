Shows you how to write integration tests for a plugin. This tutorial demonstrates how to write such tests for both UI elements provided by the plugin itself, as well as the host application.



https://developer.atlassian.com/display/JIRADEV/Plugin+Tutorial+-+Writing+Integration+Tests+for+your+JIRA+plugin